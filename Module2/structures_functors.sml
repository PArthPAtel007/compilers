signature ORD =
	sig
		type t
		val le : t -> t -> bool
	end


structure IntOrd : ORD =
struct

	type t = int;
	fun le (x:int) (y:int) = x <= y ;
	
end


structure RealOrd : ORD =
struct

	type t = real;
	fun le (x:real) (y:real) = x <= y ;
	
end



functor QSortF( X: ORD ) = struct
	
	
	fun QSort [] = []
	|	QSort (y::ys) = let val  ( ge, l ) = List.partition (X.le y ) ys in
						QSort l @ [y] @ QSort ge end

end


structure IntSort = QSortF (IntOrd);
structure RealSort = QSortF (RealOrd);

IntSort.QSort [4,6,8,3,6,5,7,2,4,24234,5454,545,45,434,4342];
RealSort.QSort [2.4,1.4234,4342.42324,5.55555];
