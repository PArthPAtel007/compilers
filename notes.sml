(* comments *)

structure name : sigName =
struct
	(* Body *)
end

signature name =
sig
	(* Body *)
end

val aString     = "Hello  world"
val aChar       = #"c"
val aBool       = true
val anotherBool = false
val anInt       = 42
val aReal       = 42.0
val aList       = [1,2,3]

val aPair       = ("the answer to life, universe, and everything", 42)
val aTriple     = (1, 2.4, "hello")              (* non-homogeneous *)
val aUnit       = () (* empty product type *)

(*

	SML is strongly typed, but there is no need to give type explicitely. It can infer by itself.

*)

type vector2d = real * real


val unitx : vector2d = (1.0,0.0)



(* function defenition *)
fun fact n = if n <= 0 then 1
	     else n * fact (n - 1) (* Notice the recursive use of fact *)



(*
uncurry
val add : int * int -> int
*)
fun add (u,v) = u + v

(*
curry
val addp : int -> (int -> int)
*)
fun addp u v  = u + v


fun curry   f  x y  = f (x,y)	(* convert to curried form   *)
fun uncurry f (x,y) = f x y    (* convert to uncurried form *)


(*list of functions*)
val someIntFunctions = [addp 1, addp 2, addp 3]



(*lambda calculus, anonymous function*)
val increment = fn x => x + 1


(*these are simillar functions!*)
fun f a b c = e
fun f a b   = fn c => e
fun f a     = fn b => fn c => e
val f       = fn a => fn b => fn c => e


(*(x::xs)   parenthesis are must*)
fun map f []         = []
  | map f (x :: xs)  = f x :: map f xs



(*
foldl takes
UNCURRIED
function
*)

(*
foldl - folds from left
foldr - folds from right
If the operator o is not commutative this can be different.
*)

val productFn = foldl (fn (x,y) => x*y) 1
val tmp = productFn [1,2,3,4,5,6,7,8,9,10]


(* using function composition o *)
val fct = prod o enum 1


(*algebraic data type*)
datatype Colour = Red | Blue | Green