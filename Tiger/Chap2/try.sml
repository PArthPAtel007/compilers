fun curry f x = fn y => f (x,y)
fun uncurry f = fn (x,y) => f x y

fun addP (x,y) = x+y
val fnn = curry addP

fnn 4 5
